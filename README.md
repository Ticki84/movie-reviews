# PPII «Projet Pluridisciplinaire d'Informatique Intégrative» (2019-2020)
## Analyse des sentiments à partir de critiques de films

### How to generate a Makefile

We have included a CMakeLists.txt file within the project, you can automatically generate a Makefile for many compilers and OS!

You must first install CMake on your computer, e.g. for Linux:

```
sudo apt-get install cmake
```

Please check that your CMake version is 3.10 or above, otherwise you can change cmake_minimum_required(VERSION X.XX) to your version in CMakeLists.txt files in both project's root and src/ directories, but it's not garanteed to be compatible.

To get the list of all available compilers, open a terminal and enter:

```
cmake -G
```

Then open a terminal in the project's root directory and enter:

```
mkdir Release

cd Release

cmake -DCMAKE_BUILD_TYPE=Release -G "<Your compiler>" ..

cmake --build .
```

All compiled files will be in the Release subdirectory.

### How to use

```
This program can get the global feeling of a movie review.

  Use:
    ./main [-h | --help]
    ./main (a | analyse) <rev> [(-i <ign> | --ignore <ign>)] [(-c | --case)] [-s | --simple] [-f | --factor] [(-n | --neutral)] [com]
    ./main (w | words) <rev> [(-i <ign> | --ignore <ign>)] [(-c | --case)] [-s | --simple] [-f | --factor]
    ./main (v | validate) <rev> [(-k <k> | --k <k>)] [(-b | --blocs)] [(-i <ign> | --ignore <ign>)] [(-c | --case)] [-s | --simple] [-f | --factor] [(-n | --neutral)]
    ./main (t | tests)

  Commands:
    -h, --help                            Show help and exit.
    a, analyse                            Analyse a comment.
    w, words                              Ask on loop for words and display their score.
    v, validate                           Do a k-cross validation test.
    t, tests                              Do some unit tests.

  Options:
    -i <ign>, --ignore <ign>              Ignore words in <ign> file [default: NULL].
    -c, --case                            If set, enable case sensitivity for comment, reviews and ignored words.
    -n, --neutral                         If set, all words not found will be counted as neutral.
    -k <k>, --k <k>                       Set the number of samples for k-cross [default: 2].
    -b, --blocs                           If set, show mean error for each k.
    -s, --simple                          If set, use the naive version of hach table.
    -f, --factor                          If set, show the load factor of hash table.

  Variables:
    rev                                   Path to reviews file.
    ign                                   Path to file with ignored words.
    com                                   The comment to evaluate.
    k                                     The number of samples (k-cross).
```

### Examples

Here are some commands you can enter in your terminal:

```
./main
./main -h
./main a ../data/movie_reviews.txt
./main analyse ../data/movie_reviews.txt -i ../data/en_stopwords.txt "It made me want to poke out my eyeballs" --simple
./main a ../data/critiques_presse.txt "J'ai adoré ce film" -i ../data/fr_stopwords.txt
./main a ../data/critiques_presse.txt --factor --neutral -s -c -i ../data/fr_stopwords.txt
./main words ../data/movie_reviews.txt
./main w ../data/movie_reviews.txt --case -s -i ../data/en_stopwords.txt -f
./main v ../data/movie_reviews.txt
./main validate ../data/movie_reviews.txt -f -k 10 -n --blocs -i ../data/en_stopwords.txt -s --case
./main t
```

### Project organization

- README.md is the file you're currently reading.

- CMakeLists.txt is explained in the How to generate a Makefile section above.

- .gitignore is a file containing all (Git) ignoring rules.

- DOW.pdf is the subject of the project.

- src/ folder contains all source files.

- src/scraper/ folder contains our Node.js AlloCiné scraper.

- data/ folder contains text files you can use with the program.

- rapport/ folder contains the report.

### Contributors

- Florent Caspar

- Marie-Astrid Chanteloup

- Théotime Forgnet