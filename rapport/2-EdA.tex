%Pour rester cohérent avec l'enseble du document, lorsque vous voulez changer de paragraphe sans créer de nouvelle section, utilisez la commande \vskip 0,25cm

\section{Table de Hachage}
\subsection{Structure}

Une \textbf{table de hachage} est une structure de données basée sur le principe d'\textbf{association entre une clef et une ou plusieurs valeurs}. \cite{wiki:hashEnglish} À partir de cette clef, on calcule un \textbf{indice} (aussi nommé \textbf{nombre ou valeur de hachage}) qui permet de répartir les association clef-valeurs dans des \textbf{alvéoles} de même indice. Lorsque que deux clefs ont le même nombre de hachage, on parle de \textbf{collision}. Pour une fonction de hachage bien choisie, la complexité temporelle de l'accès en mémoire des données et l'ajout d'une données est en moyenne en $O(1)$.
\vskip 0.25cm

Plusieurs structures permettent d'implémenter la structure de table de hachage. L'élément commun est l'utilisation d'un \textbf{tableau} pour répartir les \textbf{alvéoles}. Ensuite, la représentation des alvéoles peut varier, nécessitant différentes gestions des collisions.
\vskip 0.25cm
Une première représentation des alvéoles peut se faire grâce à des \textbf{listes chaînées}. Les associations clef-valeurs sont donc rangées dans la liste obtenue à la case du tableau d'indice égal au nombre de hachage de la clef. Lorsque l'on obtient une collision, la nouvelle valeur est insérée à la fin de la liste chaînée (cf fig. \ref{fig:repreHash}). Un de ces avantages majeurs est que la table peut contenir un \textbf{nombre illimité de valeurs}.

\begin{figure}[ht]
    \centering
    \includegraphics[scale = 0.5]{imageEDA/repréHash.png}
    \caption{Représentation d'une table de Hachage avec alvéoles en listes chaînées \cite{wiki:hashEnglish}}
    \label{fig:repreHash}
\end{figure}

Un autre représentation des alvéoles, dite en \textbf{adressage ouvert} (\textit{Open addressing} en anglais), est d'utiliser les alvéoles uniquement comme des boites pour les associations clef-valeurs. En cas de collision, on cherche la première boite vide à la suite de celle initialement prévue (cf fig. \ref{fig:openAdress}).

\begin{figure}[ht]
    \centering
    \includegraphics[scale = 0.5]{imageEDA/openAdress.png}
    \caption{Représentation d'une table de Hachage avec alvéoles en adressage ouvert \cite{wiki:hashEnglish}}
    \label{fig:openAdress}
\end{figure}

\vskip 0.25cm

Dans le cadre de ce projet, nous avons utilisé la première représentation des alvéoles et une variante sur celle-ci. 


\subsection{Fonction de Hachage}
Le choix de la \textbf{fonction de hachage} est importante pour permettre de \textbf{limiter au maximum l'espace mémoire engagé} par la table de hachage tout en \textbf{minimisant le temps de recherche} pour un élément. Une bonne fonction de hachage doit donc au moins permettre une \textbf{répartition uniforme} des données en fonction de la taille du tableau.
\vskip 0.25cm
En effet si la répartition n'est pas uniforme, une grande partie de données peut être réunie dans une même alvéoles, la complexité temporelle pour l'accès à une donnée monte donc à $O(n)$ où $n$ est le nombre de données.\\

\newpage


\section{K-Fold Cross-Validation}

En statistique, la \textbf{validation croisée} (\textit{Cross-validation} en anglais) est une méthode d'\textbf{estimation de fiabilité d'un modèle} par \textbf{échantillonage des données} \cite{wiki:cross-Validation}.
\vskip 0.25cm
La \textbf{k-fold cross-validation} fait partie de la catégorie \textbf{non-exhaustive} de ces méthodes, c'est à dire qui ne prend pas en compte toutes les répartitions possibles du set de données original.
\vskip 0.25cm
Cette méthode se décompose en trois étapes : le \textbf{mélange des données}, la \textbf{répartition en $k$ sous-set de données}, et la \textbf{vérification du modèle pour ces $k$ sous sets}.
\vskip 0.25cm
Dans un premier temps, les données sont réorganisées de \textbf{manière aléatoire}, pour que leur ordre initial, qui pourrait être lié classement, n'influe pas dans les étapes suivantes.

\begin{figure}[ht]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{imageEDA/tab1.png}
        \caption{Données initialement ordonnées}
        \label{fig:orderedData}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{imageEDA/tab2.png}
        \caption{Données mélangées}
        \label{fig:mixedData}
    \end{subfigure}
    \label{fig:mélange}
    \caption{Étape 1 : mélange des données pour $n=12$}
\end{figure}

\vskip 0.25cm
Dans un second temps, les données sont séparées en \textbf{$k$ sous-sets} de données de \textbf{tailles similaires}.
\vskip 0.25cm
Enfin, pour chacun des sous-sets, \textbf{on vérifie le modèle} choisi en utilisant les autres sous-sets comme données.

\begin{figure}[h]
    \centering
    \begin{subfigure}{0.3\textwidth}
    \includegraphics[width=\textwidth]{imageEDA/tab3.png}
    \caption{Test sur le premier sous-set}
    \label{fig:ss1}
    \end{subfigure}
    \begin{subfigure}{0.3\textwidth}
    \includegraphics[width=\textwidth]{imageEDA/tab5.png}
    \caption{Test sur le second sous-set}
    \label{fig:ss2}
    \end{subfigure}
    \begin{subfigure}{0.3\textwidth}
    \includegraphics[width = \textwidth]{imageEDA/tab6.png}
    \caption{Test sur le troisième sous-set}
    \label{fig:ss3}
    \end{subfigure}
    \label{fig:step2_3}
    \caption{\centering Étapes 2 et 3 : division des $n$ éléments en $k=3$ sous-sets\\ et test sur chacun des sous-sets}
\end{figure}

Pour évaluer les écarts, on utilise \textbf{l'écart moyen quadratique} de forme : 
\begin{equation}
    MSE=\frac{1}{n}\sum_{i=0}^{n-1}(x_i-e_i)^2
    \label{mse}
\end{equation}
où $(x_i)_{i\epsilon \llbracket 0,n-1\rrbracket}$ représente les valeurs réelles et $(t_i)_{i\epsilon\llbracket0,n-1\rrbracket}$ les valeurs estimées grâce au modèle.\\
\vskip 0.25cm
Quelques valeurs de $k$ sont recommandées pour l'utilisation de la k-fold cross-validation : 2, 10 et le nombre total de données.