#ifndef ANALYSE_LECTUREARGS_H
#define ANALYSE_LECTUREARGS_H

#include <stddef.h>
#include "doubleTableHachage.h"

typedef struct argsParser {
    size_t size;
    int analyse;
    int words;
    char *fichierReviews;
    int ignore;
    char *fichierIgnore;
    int caseSensitive;
    int neutral;
    char *commentaire;
    int validation;
    int k;
    int afficherBlocs;
    table_t typeTable;
    int tests;
    int facteurCompression;
} argsParser;
typedef argsParser* ArgsParser;

ArgsParser getArgs(int argc, char *argv[]);
int isFile(const char *path);
void delete(ArgsParser args);

#endif //ANALYSE_LECTUREARGS_H
