#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "listSimpleChain.h"

Liste list_creer() {
    Liste c = malloc(sizeof(cellule));
    c->val = NULL;
    c->suiv = NULL;
    return c;
}

int list_estVide(Liste L) {
    return L->val == NULL;
}

Liste list_ajoutTete(Liste L, ELEMENT e) {
    if (list_rech(L, e->clef) != NULL) return L;
    if (list_estVide(L)) {
        L->val = e;
        return L;
    }
    else {
        Liste c = malloc(sizeof(cellule));
        c->val = e;
        c->suiv = L;
        return c;
    }
}

void list_ajoutFin(Liste L, ELEMENT e) {
    if (list_rech(L, e->clef) != NULL) return;
    if (list_estVide(L)) {
        L->val = e;
    }
    else {
        Liste c = malloc(sizeof(cellule));
        c->val = e;
        c->suiv = NULL;
        Liste nx = L;
        while (nx->suiv != NULL) {
            nx = nx->suiv;
        }
        nx->suiv = c;
    }
}

Liste list_ajoutN(Liste L, ELEMENT e, int n) {
    if (list_rech(L, e->clef) != NULL) return L;
    if (n < 0) {
        printf("invalid index");
        exit(1);
    }
    if (n == 0) return list_ajoutTete(L, e);
    Liste nx = L;
    for (int i = 0; i < n - 1; i++) {
        if (nx->suiv != NULL) {
            nx = nx->suiv;
        }
        else {
            printf("invalid index");
            exit(1);
        }
    }
    Liste c = malloc(sizeof(cellule));
    c->val = e;
    c->suiv = nx->suiv;
    nx->suiv = c;
    return L;
}

ELEMENT list_rechN(Liste L, Clef cl, int n) {
    if (n < 0) {
        printf("invalid index");
        exit(1);
    }
    Liste nx = L;
    for (int i = 0; i < n; i++) {
        if (!list_estVide(nx) && !strcmp(nx->val->clef, cl)) return nx->val;
        if (nx->suiv != NULL) {
            nx = nx->suiv;
        }
        else break;
    }
    return NULL;
}

ELEMENT list_rech(Liste L, Clef cl) {
    Liste nx = L;
    while(1) {
        if (!list_estVide(nx) && !strcmp(nx->val->clef, cl)) return nx->val;
        if (nx->suiv != NULL) {
            nx = nx->suiv;
        }
        else break;
    }
    return NULL;
}

Liste list_remove(Liste L, Clef cl) {
    if (list_estVide(L)) return L;
    if (!strcmp(L->val->clef, cl)) {
        Liste tmp = L->suiv;
        if (tmp != NULL) {
            free(L->val->clef);
            free(L->val);
            free(L);
            return tmp;
        }
        else {
            free(L->val->clef);
            free(L->val);
            L->val = NULL;
            return L;
        }
    }
    Liste pr = L;
    while(pr != NULL) {
        if (pr->suiv != NULL && !strcmp(pr->suiv->val->clef, cl)) {
            Liste nx = pr->suiv->suiv;
            free(pr->suiv->val->clef);
            free(pr->suiv->val);
            free(pr->suiv);
            pr->suiv = nx;
            break;
        }
        pr = pr->suiv;
    }
    return L;
}

Liste list_removeN(Liste L, int n) {
    if (n < 0) {
        printf("invalid index");
        exit(1);
    }
    if (n == 0) {
        Liste tmp = L->suiv;
        if (tmp != NULL) {
            free(L->val->clef);
            free(L->val);
            free(L);
            return tmp;
        }
        else {
            free(L->val->clef);
            free(L->val);
            L->val = NULL;
            return L;
        }
    }
    else {
        Liste pr = L;
        for (int i = 0; i < n - 1; i++) {
            if (pr->suiv != NULL) {
                pr = pr->suiv;
            }
            else {
                printf("invalid index");
                exit(1);
            }
        }
        if (pr->suiv == NULL) {
            printf("invalid index");
            exit(1);
        }
        else {
            Liste nx = pr->suiv->suiv;
            free(pr->suiv->val->clef);
            free(pr->suiv->val);
            free(pr->suiv);
            pr->suiv = nx;
        }
        return L;
    }
}

void list_affiche(ELEMENT e) {
    printf("%s -> %.2f;%d\n", e->clef, e->valeur, e->occurence);
}

void list_visualiser(Liste L) {
    if (list_estVide(L)) {
        printf("(null)\n\n");
        return;
    }
    Liste nx = L;
    while(nx != NULL) {
        list_affiche(nx->val);
        nx = nx->suiv;
    }
    printf("\n");
}
