#include <stdio.h>
#include <stdlib.h>
#include "lectureLigne.h"
#include "compression.h"


double facteurCompressionSimple(TableHachage t) {
    int nbMot = 0;
    for (int i = 0; i < t->tailleTableau; i++) {
            Liste L = *(t->tableauListe + i);

            Liste nx = L;
            while(nx != NULL) {
                nbMot = nbMot +1;
                nx = nx->suiv;
            }
        
        }
    return (float)(nbMot)/(float)t->tailleTableau;

}


double facteurCompressionDouble(DoubleTableHachage dt) {
    int nbMot = 0;
    for(int i=0 ; i < dt->tailleDoubleTableau; i++) {
        TableHachage t = *(dt->tableauHachage+i);
       
        for (int j = 0; j < t->tailleTableau; j++) {
           
            Liste L = *(t->tableauListe + j);
            if (!list_estVide(L)) {
                
                Liste nx = L;
                while(nx != NULL) {
                    nx = nx->suiv;
                    nbMot = nbMot +1;
                }
            
            }
        }
    }
    return (float)nbMot/(float)(dt->tailleDoubleTableau * dt->tableauHachage[0]->tailleTableau);

}