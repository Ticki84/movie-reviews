#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include "lectureArgs.h"
#include "exceptionHandler.h"

#define BUFFERSIZE 512

ArgsParser getArgs(int argc, char *argv[]) {
    ArgsParser args = malloc(sizeof(argsParser));
    args->size = BUFFERSIZE;
    args->analyse = 0;
    args->words = 0;
    args->fichierReviews = malloc(sizeof(char) * BUFFERSIZE);
    args->ignore = 0;
    args->fichierIgnore = malloc(sizeof(char) * BUFFERSIZE);
    args->caseSensitive = 0;
    args->neutral = 0;
    args->commentaire = NULL;
    args->validation = 0;
    args->k = 0;
    args->afficherBlocs = 0;
    args->typeTable = doubleTable_t;
    args->tests = 0;
    args->facteurCompression = 0;

    if (argc == 1) helpPrint(EXIT_SUCCESS);
    for (int i=1; i<argc; i++) {
        // Help
        if (strcmp("-h", argv[i]) == 0 || strcmp("--help", argv[i]) == 0) {
            delete(args);
            helpPrint(EXIT_SUCCESS);
        }
        // Analyse
        else if (strcmp("a", argv[i]) == 0 || strcmp("analyse", argv[i]) == 0) {
            args->analyse = 1;
            if (i+1 == argc) {
                delete(args);
                helpPrint(EXIT_FAILURE); // Si a est entré sans le fichier de reviews
            }
            if (!isFile(argv[i+1])) {
                delete(args);
                errPrintCat(EXIT_FAILURE, "Can't open file ", argv[i+1]);
            }
            strcpy(args->fichierReviews, argv[i+1]);
            i++; // On saute un mot
        }
        // Display
        else if (strcmp("w", argv[i]) == 0 || strcmp("words", argv[i]) == 0) {
            args->words = 1;
            if (i+1 == argc) {
                delete(args);
                helpPrint(EXIT_FAILURE); // Si w est entré sans le fichier de reviews
            }
            if (!isFile(argv[i+1])) {
                delete(args);
                errPrintCat(EXIT_FAILURE, "Can't open file ", argv[i+1]);
            }
            strcpy(args->fichierReviews, argv[i+1]);
            i++; // On saute un mot
        }
        // Validation
        else if (strcmp("v", argv[i]) == 0 || strcmp("validate", argv[i]) == 0) {
            args->validation = 1;
            if (i+1 == argc) {
                delete(args);
                helpPrint(EXIT_FAILURE); // Si v est entré sans le fichier de reviews
            }
            if (!isFile(argv[i+1])) {
                delete(args);
                errPrintCat(EXIT_FAILURE, "Can't open file ", argv[i+1]);
            }
            strcpy(args->fichierReviews, argv[i+1]);
            i++; // On saute un mot
        }
        // Tests unitaires
        else if (strcmp("t", argv[i]) == 0 || strcmp("tests", argv[i]) == 0) {
            if (i != 1 || argc > 2) { // Si t est entré avec d'autres options
                delete(args);
                helpPrint(EXIT_FAILURE); // Si a est entré sans le fichier de reviews
            }
            args->tests = 1;
        }
        // Ignore
        else if (strcmp("-i", argv[i]) == 0 || strcmp("--ignore", argv[i]) == 0) {
            args->ignore = 1;
            if (i+1 == argc) {
                delete(args);
                helpPrint(EXIT_FAILURE); // Si i est entré sans le fichier de stopwords
            }
            strcpy(args->fichierIgnore, argv[i+1]);
            i++; // On saute un mot
        }
        // Case sensitive
        else if (strcmp("-c", argv[i]) == 0 || strcmp("--case", argv[i]) == 0) args->caseSensitive = 1;
        // Neutral
        else if (strcmp("-n", argv[i]) == 0 || strcmp("--neutral", argv[i]) == 0) args->neutral = 1;
        // k
        else if (strcmp("-k", argv[i]) == 0 || strcmp("--k", argv[i]) == 0) {
            if (i+1 == argc) { // Si l'option k est entré sans le paramètre
                delete(args);
                helpPrint(EXIT_FAILURE);
            }
            for (unsigned int j = 0; j < strlen(argv[i+1]); j++) { // Si le k entré n'est pas un nombre
                if (!isdigit(argv[i+1][j])) {
                    delete(args);
                    helpPrint(EXIT_FAILURE);
                }
            }
            args->k = atoi(argv[i+1]);
            if (args->k < 2) {
                delete(args);
                helpPrint(EXIT_FAILURE);
            }
            i++; // On saute un mot
        }
        // Afficher les blocs
        else if (strcmp("-b", argv[i]) == 0 || strcmp("--blocs", argv[i]) == 0) args->afficherBlocs = 1;
        // Utiliser la table de hachage naïve
        else if (strcmp("-s", argv[i]) == 0 || strcmp("--simple", argv[i]) == 0) args->typeTable = simpleTable_t;
        // Afficher le facteur de compression
        else if (strcmp("-f", argv[i]) == 0 || strcmp("--factor", argv[i]) == 0) args->facteurCompression = 1;
        // Commentaire
        else {
            if (args->commentaire != NULL) {
                delete(args);
                helpPrint(EXIT_FAILURE); // Le commentaire a déjà été lu
            }
            args->commentaire = malloc(sizeof(char) * BUFFERSIZE);
            strcpy(args->commentaire, argv[i]);
        }
    }
    // Règles
    if ((args->analyse && (args->words || args->validation || args->tests)) || (args->words && (args->validation || args->tests)) || (args->validation && args->tests)) {
        delete(args);
        helpPrint(EXIT_FAILURE);
    }
    if (args->commentaire && (args->words || args->validation)) {
        delete(args);
        helpPrint(EXIT_FAILURE);
    }
    if (args->neutral && args->words) {
        delete(args);
        helpPrint(EXIT_FAILURE);
    }
    if ((args->k || args->afficherBlocs) && (args->words || args->analyse)) {
        delete(args);
        helpPrint(EXIT_FAILURE);
    }
    if (!(args->analyse || args->words || args->validation || args->tests)) { // Si aucune des commandes n'est demandée
        delete(args);
        helpPrint(EXIT_FAILURE);
    }
    if (args->k == 0) args->k = 2; // Si k n'a pas été changé, on l'initialise
    return args;
}

int isFile(const char *path) {
    struct stat path_stat = {};
    if (path != NULL) stat(path, &path_stat);
    else errPrint(EXIT_FAILURE, "Invalid path");
    return S_ISREG(path_stat.st_mode);
}

void delete(ArgsParser args) {
    free(args->fichierReviews);
    free(args->fichierIgnore);
    if (args->commentaire != NULL) free(args->commentaire);
    free(args);
}