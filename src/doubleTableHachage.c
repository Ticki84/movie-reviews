#include <string.h>
#include <stdlib.h>
#include "doubleTableHachage.h"
#define TAILLELISTE 12

int hachage_square(char *s, int mod)
{
    int i = 0, cpt = 0;

    while (s[i] != '\0')
    {
        cpt += (int)s[i]*(int)s[i];
        i++;
    }
    cpt += strlen(s);
    cpt = cpt % mod;
    return cpt;
}


DoubleTableHachage doubleTableHachage_creer(int tailleDoubleTableau) {
    DoubleTableHachage dt = malloc(sizeof(doubleTableHachage));
    dt->tableauHachage = malloc(sizeof(tableHachage) * tailleDoubleTableau);
    for (int i=0; i < tailleDoubleTableau; i++) {
        *(dt->tableauHachage+i)= tableHachage_creer(TAILLELISTE);
    }
    dt->tailleDoubleTableau = tailleDoubleTableau;
    return dt;
}


ELEMENT doubleTableHachage_get(Clef c, DoubleTableHachage dt) {
    int h = hachage_square(c, dt->tailleDoubleTableau);
    TableHachage t = *(dt->tableauHachage + h);
    return tableHachage_get(c,t);
}


void doubleTableHachage_ajout(ELEMENT e, DoubleTableHachage dt) {
    int h = hachage_square(e->clef, dt->tailleDoubleTableau);
    TableHachage t = *(dt->tableauHachage + h);
    tableHachage_ajout(e,t);
}

void doubleTableHachage_supprime(Clef c, DoubleTableHachage dt) {
    int h = hachage_square(c, dt->tailleDoubleTableau);
    TableHachage t = *(dt->tableauHachage+h);
    tableHachage_supprime(c,t);
}

void doubleTableHachage_visualiser(DoubleTableHachage dt) {
    for(int i=0 ; i < dt->tailleDoubleTableau; i++) {
        tableHachage_visualiser(*(dt->tableauHachage+i));
    }
}

void doubleTableHachage_supprimer(DoubleTableHachage dt) {
    for (int i =0; i < dt->tailleDoubleTableau; i++) {
        TableHachage t = *(dt->tableauHachage+i);
        tableHachage_supprimer(t);
    }
    free(dt->tableauHachage);
    free(dt);
}
