#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "words.h"
#include "exceptionHandler.h"
#include "lectureLigne.h"
#include "motsVides.h"
#include "compression.h"


void words(ArgsParser args) {
    if (args->typeTable == doubleTable_t) { // Si la table de hachage est la double table de hachage
        DoubleTableHachage t = remplirDoubleTable(args->fichierReviews, args->caseSensitive);
        if (args->ignore) supprimerMotsVidesDouble(t, args->fichierIgnore, args->caseSensitive); // On supprime les mots vides si l'option est active
        if (args->facteurCompression) printf("The load factor is %.2f\n\n", facteurCompressionDouble(t));
        args->commentaire = malloc(sizeof(char) * args->size);
        while (1) {
            printf("Please enter a word (Press return to exit):\n");
            if (!fgets(args->commentaire, args->size, stdin)) {
                doubleTableHachage_supprimer(t);
                delete(args);
                errPrint(EXIT_FAILURE, "Error while reading stdin");;
            }
            if (*(args->commentaire + strlen(args->commentaire) - 1) == '\n')
                *(args->commentaire + strlen(args->commentaire) - 1) = '\0';
            else {
                doubleTableHachage_supprimer(t);
                delete(args);
                errPrint(EXIT_FAILURE,
                         "Please increase BUFFERSIZE in lectureArgs.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
            }
            if (!args->caseSensitive) {
                int i = 0;
                while (args->commentaire[i] != '\0') {
                    args->commentaire[i] = (char) tolower(args->commentaire[i]);
                    i++;
                }
            }
            if (strcmp("", args->commentaire) == 0) break;
            ELEMENT elt = doubleTableHachage_get(args->commentaire, t);
            if (elt != NULL)
                printf("The word \"%s\" has an average value of %.2f with %d occurence(s)\n", args->commentaire,
                       elt->valeur, elt->occurence);
            else printf("The word \"%s\" was not found\n", args->commentaire);
        }
        doubleTableHachage_supprimer(t);
    }
    else { // Si c'est la table de hachage naïve, on a des redondances de code à cause du manque de polymorphisme en C :'(
        TableHachage t = remplirTable(args->fichierReviews, args->caseSensitive);
        if (args->ignore) supprimerMotsVides(t, args->fichierIgnore, args->caseSensitive); // On supprime les mots vides si l'option est active
        if (args->facteurCompression) printf("The load factor is %.2f\n\n", facteurCompressionSimple(t));
        args->commentaire = malloc(sizeof(char) * args->size);
        while (1) {
            printf("Please enter a word (Press return to exit):\n");
            if (!fgets(args->commentaire, args->size, stdin)) {
                tableHachage_supprimer(t);
                delete(args);
                errPrint(EXIT_FAILURE, "Error while reading stdin");;
            }
            if (*(args->commentaire + strlen(args->commentaire) - 1) == '\n')
                *(args->commentaire + strlen(args->commentaire) - 1) = '\0';
            else {
                tableHachage_supprimer(t);
                delete(args);
                errPrint(EXIT_FAILURE,
                         "Please increase BUFFERSIZE in lectureArgs.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
            }
            if (!args->caseSensitive) {
                int i = 0;
                while (args->commentaire[i] != '\0') {
                    args->commentaire[i] = (char) tolower(args->commentaire[i]);
                    i++;
                }
            }
            if (strcmp("", args->commentaire) == 0) break;
            ELEMENT elt = tableHachage_get(args->commentaire, t);
            if (elt != NULL)
                printf("The word \"%s\" has an average value of %.2f with %d occurence(s)\n", args->commentaire,
                       elt->valeur, elt->occurence);
            else printf("The word \"%s\" was not found\n", args->commentaire);
        }
        tableHachage_supprimer(t);
    }
}