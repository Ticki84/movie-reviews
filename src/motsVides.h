#ifndef ANALYSE_MOTSVIDES_H
#define ANALYSE_MOTSVIDES_H

#include "tableHachage.h"
#include "doubleTableHachage.h"

void supprimerMotsVides(TableHachage t, char *filePath, int caseSensitive);
void supprimerMotsVidesDouble(DoubleTableHachage dt, char *filePath, int caseSensitive);

#endif //ANALYSE_MOTSVIDES_H
