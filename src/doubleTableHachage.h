#ifndef TEST_LIST_DOUBLETABLEHACHAGE_H
#define TEST_LIST_DOUBLETABLEHACHAGE_H

#include "tableHachage.h"

typedef enum {
    simpleTable_t,
    doubleTable_t,
} table_t;

typedef struct doubleTableHachage {
    TableHachage* tableauHachage;
    int tailleDoubleTableau;
} doubleTableHachage;

typedef doubleTableHachage* DoubleTableHachage;

int hachage_square(char *s, int mod);
DoubleTableHachage doubleTableHachage_creer(int tailleDoubleTableau);
ELEMENT doubleTableHachage_get(Clef c, DoubleTableHachage dt);
void doubleTableHachage_ajout(ELEMENT e, DoubleTableHachage dt);
void doubleTableHachage_supprime(Clef c, DoubleTableHachage dt);
void doubleTableHachage_visualiser(DoubleTableHachage dt);
void doubleTableHachage_supprimer(DoubleTableHachage dt);

#endif //TEST_LIST_DOUBLETABLEHACHAGE_H