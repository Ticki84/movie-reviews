#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "tests.h"
#include "exceptionHandler.h"
#include "lectureLigne.h"
#include "calculScore.h"
#include "motsVides.h"
#include <time.h>


void tests() {
    // Lecture du fichier et validation de la table
    clock_t begin = clock();
    DoubleTableHachage t = remplirDoubleTable("../data/movie_reviews.txt", 0);
    double time_spent = (double)(clock() - begin) / CLOCKS_PER_SEC;
    printf("Double hash table filled in %fs\n", time_spent);
    begin = clock();
    TableHachage tc = remplirTable("../data/movie_reviews.txt", 1);
    time_spent = (double)(clock() - begin) / CLOCKS_PER_SEC;
    printf("Simple hash table filled in %fs\n", time_spent);

    ELEMENT elt = NULL;
    begin = clock();
    for (int i = 0; i<1000000; i++) {
        elt = doubleTableHachage_get("abcdefgh", t);
    }
    time_spent = (double)(clock() - begin) / CLOCKS_PER_SEC;
    printf("Accessed 1,000,000 elements in double in %fs\n", time_spent);
    begin = clock();
    for (int i = 0; i<1000000; i++) {
        elt = tableHachage_get("abcdefgh", tc);
    }
    time_spent = (double)(clock() - begin) / CLOCKS_PER_SEC;
    printf("Accessed 1,000,000 elements in simple in %fs\n", time_spent);
    // Les variations sont minimes entre les deux fonctions de hachage

    // Ces valeurs ont été obtenus via Notepad++ en mode Mot entier uniquement
    // Permet de vérifier le remplissage des deux types de tables (naïve et double hachage), le respect de la casse et la bonne restitution/recherche des valeurs de la table
    printf("\nWith case sensitivity:\n");
    elt = tableHachage_get("fantastic", tc);
    if (elt != NULL) printf("%s: The word \"%s\" has an average value of %.2f with %d/12 occurence(s)\n", elt->occurence == 12 ? "OK" : "WARNING", "fantastic", elt->valeur, elt->occurence);
    else printf("WARNING: The word \"%s\" was not found\n", "fantastic");
    elt = tableHachage_get("horrible", tc);
    if (elt != NULL) printf("%s: The word \"%s\" has an average value of %.2f with %d/11 occurence(s)\n", elt->occurence == 11 ? "OK" : "WARNING", "horrible", elt->valeur, elt->occurence);
    else printf("WARNING: The word \"%s\" was not found\n", "horrible");
    elt = tableHachage_get("ok", tc);
    if (elt == NULL) printf("OK: The word \"%s\" was not found\n", "ok");
    else printf("WARNING: The word \"%s\" has an average value of %.2f with %d/3 occurence(s)\n", "ok", elt->valeur, elt->occurence);
    printf("Without case sensitivity:\n");
    elt = doubleTableHachage_get("fantastic", t);
    if (elt != NULL) printf("%s: The word \"%s\" has an average value of %.2f with %d/13 occurence(s)\n", elt->occurence == 13 ? "OK" : "WARNING", "fantastic", elt->valeur, elt->occurence);
    else printf("WARNING: The word \"%s\" was not found\n", "fantastic");
    elt = doubleTableHachage_get("horrible", t);
    if (elt != NULL) printf("%s: The word \"%s\" has an average value of %.2f with %d/12 occurence(s)\n", elt->occurence == 12 ? "OK" : "WARNING", "horrible", elt->valeur, elt->occurence);
    else printf("WARNING: The word \"%s\" was not found\n", "horrible");
    elt = doubleTableHachage_get("ok", t);
    if (elt != NULL) printf("%s: The word \"%s\" has an average value of %.2f with %d/3 occurence(s)\n", elt->occurence == 3 ? "OK" : "WARNING", "ok", elt->valeur, elt->occurence);
    else printf("WARNING: The word \"%s\" was not found\n", "ok");
    printf("We'll ignore case sensitivity to be more accurate\n");

    // Ignorer mots vides (avec/sans)
    // Vérifie aussi que les mots sont bien supprimés de la table
    double score = calculScoreDouble("It made me want to poke out my eyeballs",t,0,0);
    double score2 = calculScoreDouble("This was a nice movie",t,0,0);
    supprimerMotsVidesDouble(t, "../data/en_stopwords.txt", 0);
    double scoreStop = calculScoreDouble("It made me want to poke out my eyeballs",t,0,0);
    double scoreStop2 = calculScoreDouble("This was a nice movie",t,0,0);
    printf("\nWithout ignoring stopwords, \"It made me want to poke out my eyeballs\" has an average value of %.2f\n",score);
    printf("By ignoring stopwords, \"It made me want to poke out my eyeballs\" has an average value of %.2f\n",scoreStop);
    if (scoreStop >2.1) {printf("WARNING: Positive sentiment\n");}
    else if (scoreStop <1.9) {printf("OK: Negative sentiment\n");}
    else {printf("WARNING: Neutral sentiment\n");}
    printf("Without ignoring stopwords, \"This was a nice movie\" has an average value of %.2f\n",score2);
    printf("By ignoring stopwords, \"This was a nice movie\" has an average value of %.2f\n",scoreStop2);
    if (scoreStop2 >2.1) {printf("OK: Positive sentiment\n");}
    else if (scoreStop2 <1.9) {printf("WARNING: Negative sentiment\n");}
    else {printf("WARNING: Neutral sentiment\n");}
    if (scoreStop<score && scoreStop2>score2) printf("OK: It is much more accurate by ignoring stopwords\n");
    else printf("WARNING: It is much accurate without ignoring stopwords\n");
    printf("We'll ignore stopwords to be more accurate\n");

    // Contribution des mots
    tableHachage_supprimer(tc);
    tc = remplirTable("../data/movie_reviews.txt", 0); // On recrée la table (sans sensibilité à la casse)
    ELEMENT elt2 = NULL;
    ELEMENT elt3 = NULL;
    ELEMENT elt4 = NULL;
    ELEMENT elt5 = NULL;
    double MAX[5];
    double MIN[5];

    elt = tableHachage_get("terrible",tc);
    elt2 = tableHachage_get("horrible",tc);
    elt3 = tableHachage_get("ok",tc);
    elt4 = tableHachage_get("refreshing",tc);
    elt5 = tableHachage_get("formulaic",tc);

    if (elt != NULL) 
    {
        printf("\nThe word \"%s\" has an average value of %.2f\n", "terrible", elt->valeur );
        MAX[0]=elt->valeur;
        MIN[0]=elt->valeur;
    }
    else {
        printf("WARNING: The word \"%s\" was not found\n", "terrible");
        MAX[0]=0;
        MIN[0]=5;
    }
    if (elt2 != NULL) 
    {
        printf("The word \"%s\" has an average value of %.2f\n", "horrible", elt2->valeur );
        MAX[1]=elt2->valeur;
        MIN[1]=elt2->valeur;
    }
    else {
        printf("WARNING: The word \"%s\" was not found\n", "horrible");
        MAX[1]=0;
        MIN[1]=5;
    }
    if (elt3 != NULL) 
    {
        printf("The word \"%s\" has an average value of %.2f\n", "ok", elt3->valeur );
        MAX[2]=elt3->valeur;
        MIN[2]=elt3->valeur;
    }
    else {
        printf("WARNING: The word \"%s\" was not found\n", "ok");
        MAX[2]=0;
        MIN[2]=5;
    }
    if (elt4 != NULL) 
    {
        printf("The word \"%s\" has an average value of %.2f\n", "refreshing", elt4->valeur );
        MAX[3]=elt4->valeur;
        MIN[3]=elt4->valeur;
    }
    else {
        printf("WARNING: The word \"%s\" was not found\n", "refreshing");
        MAX[3]=0;
        MIN[3]=5;
    }
    if (elt5 != NULL) 
    {
        printf("The word \"%s\" has an average value of %.2f\n", "formulaic", elt5->valeur );
        MAX[4]=elt5->valeur;
        MIN[4]=elt->valeur;
    }
    else {
        printf("WARNING: The word \"%s\" was not found\n", "formulaic");
        MAX[4]=0;
        MIN[4]=5;
    }
    int posMin=0;
    int posMax=0;
    for (int i=0; i<4; i++)
    {
      if(MAX[i]>MAX[posMax]) posMax=i;
      if(MIN[i]<MIN[posMin]) posMin=i;
    }
    printf("\nBetween  terrible ,  horrible ,  ok ,  refreshing , formulaic , the most positive one is \"refreshing\" with a score of %.2f  and the most negative one is \"horrible\" with a score of %.2f \n ", MAX[posMax],MIN[posMin]);
    // Validation du score


    // Mots inconnus comme poids neutre (avec/sans)
    printf("\nTests for unknown words (weight of neutral):\n");
    double scoreUnknown = calculScoreDouble("supercali", t,0,1);
    printf("The word \"supercali\" is unknown, its value is : not set (default); %.2f (with neutral option)\n", scoreUnknown);

    scoreUnknown=calculScoreDouble("It made me want to poke out my eyeballs", t, 0, 0);
    double scoreUnknown1=calculScoreDouble("It made me want to poke out my eyeballs supercali", t, 0, 0);
    double scoreUnknown2=calculScoreDouble("It made me want to poke out my eyeballs supercali", t, 0, 1);
    printf("For the original comment \"It made me want to poke out my eyeballs\", the score of the comment is: %.2f\n", scoreUnknown);
    printf("For the comment \"It made me want to poke out my eyeballs supercali\", the score (without neutral option) is: %.2f; the unknown word had no influence\n", scoreUnknown1);
    printf("For the comment \"It made me want to poke out my eyeballs supercali\", the score (with neutral option) is: %.2f; the unknown word had an influence\n", scoreUnknown2);
    printf("We'll ignore unknown words to be more accurate\n");


    // Avec Valgrind, il n'y a pas de fuite mémoire donc la table de hachage est bien supprimée
    doubleTableHachage_supprimer(t);
    tableHachage_supprimer(tc);
}