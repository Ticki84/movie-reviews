#ifndef ANALYSE_KCROSS_H
#define ANALYSE_KCROSS_H

#include "tableHachage.h"
#include "doubleTableHachage.h"
#include "lectureArgs.h"

void kValidation(ArgsParser args);
int nbLignes(char* filePath);
int* randomization(int totalSize);
void newOrderCopy(char* pathOld, char* pathNew, int* order, int sizeTotal);
TableHachage remplirTableValidation(char *filePath, int caseSensitive, int blockSize, int outBlock);
DoubleTableHachage remplirDoubleTableValidation(char *filePath, int caseSensitive, int blockSize, int outBlock);
double ecartMoyen(char* filePath, TableHachage table, int caseSensitive, int countNeutral, int outBlock, int blockSize);
double ecartMoyenDouble(char* filePath, DoubleTableHachage table, int caseSensitive, int countNeutral, int outBlock, int blockSize);

#endif //ANALYSE_KCROSS_H
