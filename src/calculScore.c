#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "calculScore.h"

#define WORDSIZE 46
#define NEUTRALSCORE 2.0


double calculScore(const char *commentaire, TableHachage table, int caseSensitive, int countNeutral) {
    double score = NEUTRALSCORE;
    int nbMots = 0;
    int i = 0;
    char *mot;
    mot = malloc(WORDSIZE * sizeof(char)); //taille acceptable pour un mot
    int warning = 0;
    while (commentaire[i] != '\0') {
        int k = 0;

        while (k < WORDSIZE && (commentaire[i] != ' ') && (commentaire[i] != '\t') && (commentaire[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne
            mot[k] = (char)(caseSensitive ? commentaire[i] : tolower(commentaire[i]));
            i++;
            k++;
        }
        if (k < WORDSIZE) {
            mot[k] = '\0'; //on a recuperer un mot complet

            ELEMENT motTable = tableHachage_get(mot, table);
            if (motTable != NULL) {
                nbMots++;
                score = (nbMots != 1 ? ((score * (nbMots - 1) + motTable->valeur) / nbMots) : motTable->valeur);
            } else if (countNeutral) {
                nbMots++;
                score = (nbMots != 1 ? ((score * (nbMots - 1) + NEUTRALSCORE) / nbMots) : NEUTRALSCORE);
            }
        }
        else if (!warning) { // Si on ne peut pas placer le caractère \0 de fin de chaîne
            warning = 1;
            printf("Some words have been skipped because they exceed WORDSIZE (%d)\n"
                   "It isn't an error because they're usually words we don't want\n"
                   "You can still increase WORDSIZE in lectureLigne.c, kCross.c and calculScore.c\n\n", WORDSIZE);
        }

        while (commentaire[i] == ' ' || commentaire[i] == '\t') i++; //si on est dans un espace on avance
    }
    free(mot);
    return score;
}

double calculScoreDouble(const char *commentaire, DoubleTableHachage table, int caseSensitive, int countNeutral) {
    double score = NEUTRALSCORE;
    int nbMots = 0;
    int i = 0;
    char *mot;
    mot = malloc(WORDSIZE * sizeof(char)); //taille acceptable pour un mot
    int warning = 0;
    while (commentaire[i] != '\0') {
        int k = 0;

        while (k < WORDSIZE && (commentaire[i] != ' ') && (commentaire[i] != '\t') && (commentaire[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne
            mot[k] = (char)(caseSensitive ? commentaire[i] : tolower(commentaire[i]));
            i++;
            k++;
        }
        if (k < WORDSIZE) {
            mot[k] = '\0'; //on a recuperer un mot complet

            ELEMENT motTable = doubleTableHachage_get(mot, table);
            if (motTable != NULL) {
                nbMots++;
                score = (nbMots != 1 ? ((score * (nbMots - 1) + motTable->valeur) / nbMots) : motTable->valeur);
            } else if (countNeutral) {
                nbMots++;
                score = (nbMots != 1 ? ((score * (nbMots - 1) + NEUTRALSCORE) / nbMots) : NEUTRALSCORE);
            }
        }
        else if (!warning) { // Si on ne peut pas placer le caractère \0 de fin de chaîne
            warning = 1;
            printf("Some words have been skipped because they exceed WORDSIZE (%d)\n"
                   "It isn't an error because they're usually words we don't want\n"
                   "You can still increase WORDSIZE in lectureLigne.c, kCross.c and calculScore.c\n\n", WORDSIZE);
        }

        while (commentaire[i] == ' ' || commentaire[i] == '\t') i++; //si on est dans un espace on avance
    }
    free(mot);
    return score;
}