#ifndef ANALYSE_CALCULSCORE_H
#define ANALYSE_CALCULSCORE_H

#include "tableHachage.h"
#include "doubleTableHachage.h"

double calculScore(const char *commentaire, TableHachage table, int caseSensitive, int countNeutral);

double calculScoreDouble(const char *commentaire, DoubleTableHachage table, int caseSensitive, int countNeutral);

#endif //ANALYSE_CALCULSCORE_H
