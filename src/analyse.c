#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "analyse.h"
#include "exceptionHandler.h"
#include "lectureLigne.h"
#include "calculScore.h"
#include "motsVides.h"
#include "compression.h"

void analyse(ArgsParser args) {
    if (args->typeTable == doubleTable_t) { // Si la table de hachage est la double table de hachage
        DoubleTableHachage t = remplirDoubleTable(args->fichierReviews, args->caseSensitive);
        if (args->ignore) supprimerMotsVidesDouble(t, args->fichierIgnore, args->caseSensitive); // On supprime les mots vides si l'option est active
        if (args->facteurCompression) printf("The load factor is %.2f\n\n", facteurCompressionDouble(t));
        if (args->commentaire == NULL) {
            printf("Please enter your comment (Press return to exit):\n");
            args->commentaire = malloc(sizeof(char) * args->size);
            if (!fgets(args->commentaire, args->size, stdin)) {
                doubleTableHachage_supprimer(t);
                delete(args);
                errPrint(EXIT_FAILURE, "Error while reading stdin");
            }
            if (*(args->commentaire + strlen(args->commentaire) - 1) == '\n') *(args->commentaire + strlen(args->commentaire) - 1) = '\0';
            else {
                doubleTableHachage_supprimer(t);
                delete(args);
                errPrint(EXIT_FAILURE, "Please increase BUFFERSIZE in lectureArgs.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
            }
        }
        if (strcmp("", args->commentaire) != 0) {
            double score = calculScoreDouble(args->commentaire,t,args->caseSensitive,args->neutral);
            printf("The review has an average value of %.2f\n",score);
            if (score >3.1) {printf("Positive sentiment\n");}
            else if (score < 1) {printf("Negative sentiment\n");}
            else if (score <1.9 && score >= 1) {printf("Medium negative sentiment\n");}
            else if (score <=3.0 && score > 2) {printf("Medium positive sentiment\n");}
            else {printf("Neutral sentiment\n");}
        }
        doubleTableHachage_supprimer(t);
    }
    else { // Si c'est la table de hachage naïve, on a des redondances de code à cause du manque de polymorphisme en C :'(
        TableHachage t = remplirTable(args->fichierReviews, args->caseSensitive);
        if (args->ignore) supprimerMotsVides(t, args->fichierIgnore, args->caseSensitive); // On supprime les mots vides si l'option est active
        if (args->facteurCompression) printf("The load factor is %.2f\n\n", facteurCompressionSimple(t));
        if (args->commentaire == NULL) {
            printf("Please enter your comment (Press return to exit):\n");
            args->commentaire = malloc(sizeof(char) * args->size);
            if (!fgets(args->commentaire, args->size, stdin)) {
                tableHachage_supprimer(t);
                delete(args);
                errPrint(EXIT_FAILURE, "Error while reading stdin");
            }
            if (*(args->commentaire + strlen(args->commentaire) - 1) == '\n') *(args->commentaire + strlen(args->commentaire) - 1) = '\0';
            else {
                tableHachage_supprimer(t);
                delete(args);
                errPrint(EXIT_FAILURE, "Please increase BUFFERSIZE in lectureArgs.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
            }
        }
        if (strcmp("", args->commentaire) != 0) {
            double score = calculScore(args->commentaire,t,args->caseSensitive,args->neutral);
            printf("The review has an average value of %.2f\n",score);
            if (score >3.1) {printf("Positive sentiment\n");}
            else if (score < 1) {printf("Negative sentiment\n");}
            else if (score <1.9 && score >= 1) {printf("Medium negative sentiment\n");}
            else if (score <=3.0 && score > 2) {printf("Medium positive sentiment\n");}
            else {printf("Neutral sentiment\n");}
        }
        tableHachage_supprimer(t);
    }
}