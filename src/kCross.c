#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include "kCross.h"
#include "exceptionHandler.h"
#include "calculScore.h"
#include "motsVides.h"
#include "compression.h"

#define BUFFERSIZE 512
#define WORDSIZE 46
#define TABLEHACHAGESIZE 1000

void kValidation(ArgsParser args){
    //Initialisation rand
    srand(time(0));

    //Mélange du set
    int n = nbLignes(args->fichierReviews);
    int* newOrder = randomization(n);
    char* pathTemp="temp.txt";
    newOrderCopy(args->fichierReviews, pathTemp, newOrder, n);
    free(newOrder);

    //Calcul de la taille des k-blocs
    if(args->k>n){printf( "Value of k greater than number of lines in file. Chose a smaller value.\n"); return;}
    int h = n%args->k==0 ? n/args->k : n/(args->k-1); //la taille des blocs correspond soit à n=k*h soit à n=(k-1)*h+r


    //Calcul des moyennes sur les k blocs
    double meanBlock; double factor; double meanFactor = 0; double meanTotal = 0 ;

    if (args->typeTable == doubleTable_t) { // Si la table de hachage est la double table de hachage
        for (int i = 0; i < args->k; i++)
        {
            DoubleTableHachage table;
            table = remplirDoubleTableValidation(pathTemp, args->caseSensitive, h, i); //Récupération de la table de hachage sans le ieme bloc

            if (args->ignore) supprimerMotsVidesDouble(table, args->fichierIgnore, args->caseSensitive); // On supprime les mots vides si l'option est active

            meanBlock = ecartMoyenDouble(pathTemp, table, args->caseSensitive, args->neutral, i, h); //Récupération de l'erreur absolue moyenne sur le bloc i

            factor = facteurCompressionDouble(table);

            if(args->afficherBlocs){

                printf("For %ith block, the mean squared error is %.5f", i+1, meanBlock);

                if (args->facteurCompression) printf(" and the load factor is %.2f.\n", factor);

                else printf(".\n");

            }

            meanFactor = i*meanFactor/(i+1)+factor/(i+1);

            meanTotal = i*meanTotal/(i+1)+meanBlock/(i+1); //Ré-évaluation de la moyenne pour tous les blocs

            doubleTableHachage_supprimer(table); //Libération de la table sans le bloc i
        }
    }
    else { // Si c'est la table de hachage naïve, on a des redondances de code à cause du manque de polymorphisme en C :'(
        for (int i = 0; i < args->k; i++)
        {
            TableHachage table;
            table = remplirTableValidation(pathTemp, args->caseSensitive, h, i); //Récupération de la table de hachage sans le ieme bloc

            if (args->ignore) supprimerMotsVides(table, args->fichierIgnore, args->caseSensitive); // On supprime les mots vides si l'option est active

            meanBlock = ecartMoyen(pathTemp, table, args->caseSensitive, args->neutral, i, h); //Récupération de l'erreur absolue moyenne sur le bloc i

            factor = facteurCompressionSimple(table);

            if(args->afficherBlocs){

                printf("For %ith block, the mean squared error is %.5f", i+1, meanBlock);

                if (args->facteurCompression) printf(" and the load factor is %.2f.\n", factor);

                else printf(".\n");

            }

            meanFactor = i*meanFactor/(i+1)+factor/(i+1);

            meanTotal = i*meanTotal/(i+1)+meanBlock/(i+1); //Ré-évaluation de la moyenne pour tous les blocs

            tableHachage_supprimer(table); //Libération de la table sans le bloc i
        }
    }

    printf("The mean squared error on all blocks is %.5f.\n", meanTotal);
    if (args->facteurCompression) printf("The mean load factor on all blocks is %.2f.\n", meanFactor);
    if(remove(pathTemp)!=0) errPrint(EXIT_FAILURE, "The tempory file used for k-fold cross-validation could not be deleted.\n");
}

int* randomization(int totalSize){
    int* p = calloc(totalSize, sizeof(int));
    int indexZero = rand()%totalSize;
    for (int i = 1; i < totalSize; i++){
        int index = rand()%totalSize;
        while(index==indexZero || p[index]!=0){ //case déjà utilisée
            index=rand()%totalSize; //on retire un index
        }
        p[index]=i;
    }
    return p;
}

int nbLignes(char* filePath){
    //Ouverture du fichier
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);

    int count=0; char chr; //Initialisation

    chr= (char) getc(file);
    while (chr!=EOF) //pour chaque char du fichier
    {
        if(chr=='\n'){count++;} //on toggle quand on observe un saut de ligne
        chr=(char) getc(file); //on prend le char suivant
    }

    fclose(file);
    return(count);
}

void newOrderCopy(char* pathOld, char* pathNew, int* order, int sizeTotal){
    FILE* new = fopen(pathNew, "w");
    if(!new) errPrintCat(-1, "Can't open file ", pathNew);

    char lineBuffer[BUFFERSIZE];

    for (int i = 0; i < sizeTotal; i++){
        FILE* old = fopen(pathOld, "r");
        if(!old) errPrintCat(-1, "Can't open file ", pathOld);
        int newLine = order[i];
        int oldLine = 0;
        while (oldLine!=newLine && fgets(lineBuffer, sizeof(lineBuffer), old)){oldLine++;} //on avance de le fichier initial
        if (!fgets(lineBuffer, sizeof(lineBuffer), old)) errPrintCat(EXIT_FAILURE, "Error while reading ", pathOld); //on récupère la bonne ligne
        fprintf(new, "%s", lineBuffer); //que l'on copie dans le nouveau document
        fclose(old);
    }
    fclose(new);
}

TableHachage remplirTableValidation(char *filePath, int caseSensitive, int blockSize, int outBlock)
{
    //Ouverture du fichier
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);
    char content[BUFFERSIZE];
    //taille aléatoire a redefinir plus tard
    TableHachage t = tableHachage_creer(TABLEHACHAGESIZE);

    char *mot = malloc(WORDSIZE * sizeof(char)); //taille acceptable pour un mot
    int warning = 0;
    int lineCount = 0;

    while (fgets(content, sizeof(content), file) && lineCount<outBlock*blockSize) { // Lecture ligne par ligne dans la première partie du fichier

        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            tableHachage_supprimer(t);
            free(mot);
            errPrint(-1, "Please increase BUFFERSIZE in kCross.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        int i=0; //compteur pour avancer dans la ligne

        if (content[i]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            double value;
            value = (int) content[i] - '0';//on recupere la note de la review sous la forme d'un int
            i += 2; //on passe au prochain mot


            while (content[i] != '\0') {
                int k = 0;

                while (k < WORDSIZE && (content[i] != ' ') && (content[i] != '\t') && (content[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne

                    mot[k] = (char)(caseSensitive ? content[i] : tolower(content[i]));

                    i++;
                    k++;

                }
                if (k < WORDSIZE) {
                    mot[k] = '\0'; //on a recuperer un mot complet

                    ELEMENT motOld = tableHachage_get(mot, t);
                    if (motOld == NULL) {
                        ELEMENT motAjout = malloc(sizeof(struct elem));
                        motAjout->clef = mot;
                        mot = malloc(WORDSIZE * sizeof(char)); // On fait un nouveau malloc (ça évite de faire un malloc pour chaque nouveau mot)
                        motAjout->valeur = value;
                        motAjout->occurence = 1;
                        tableHachage_ajout(motAjout, t);
                    } else { //Si le mot était déjà présent
                        //On calcule et on remplace son nouveau nombre d'occurence et sa nouvelle valeur par moyenne
                        motOld->occurence++;
                        motOld->valeur = (motOld->valeur * (motOld->occurence - 1) + value) / motOld->occurence;
                    }
                }
                else if (!warning) { // Si on ne peut pas placer le caractère \0 de fin de chaîne
                    warning = 1;
                    printf("Some words have been skipped because they exceed WORDSIZE (%d)\n"
                           "It isn't an error because they're usually words we don't want\n"
                           "You can still increase WORDSIZE in lectureLigne.c, kCross.c and calculScore.c\n\n", WORDSIZE);
                }

                while (content[i] == ' ' || content[i] == '\t') i++; //si on est dans un espace on avance
            }
        }

        lineCount++; //on avance d'une ligne
    }

    while(fgets(content, sizeof(content), file) && lineCount<(outBlock+1)*blockSize){lineCount++;} //Saut du bloc outBlock

    while (fgets(content, sizeof(content), file)) { // Lecture ligne par ligne jusqu'à la fin du fichier

        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            tableHachage_supprimer(t);
            free(mot);
            errPrint(-1, "Please increase BUFFERSIZE in kCross.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        int i=0; //compteur pour avancer dans la ligne

        if (content[i]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            double value;
            value = (int) content[i] - '0';//on recupere la note de la review sous la forme d'un int
            i += 2; //on passe au prochain mot


            while (content[i] != '\0') {
                int k = 0;

                while (k < WORDSIZE && (content[i] != ' ') && (content[i] != '\t') && (content[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne

                    mot[k] = (char)(caseSensitive ? content[i] : tolower(content[i]));

                    i++;
                    k++;

                }
                if (k < WORDSIZE) {
                    mot[k] = '\0'; //on a recuperer un mot complet

                    ELEMENT motOld = tableHachage_get(mot, t);
                    if (motOld == NULL) {
                        ELEMENT motAjout = malloc(sizeof(struct elem));
                        motAjout->clef = mot;
                        mot = malloc(WORDSIZE * sizeof(char)); // On fait un nouveau malloc (ça évite de faire un malloc pour chaque nouveau mot)
                        motAjout->valeur = value;
                        motAjout->occurence = 1;
                        tableHachage_ajout(motAjout, t);
                    } else { //Si le mot était déjà présent
                        //On calcule et on remplace son nouveau nombre d'occurence et sa nouvelle valeur par moyenne
                        motOld->occurence++;
                        motOld->valeur = (motOld->valeur * (motOld->occurence - 1) + value) / motOld->occurence;
                    }
                }
                else if (!warning) { // Si on ne peut pas placer le caractère \0 de fin de chaîne
                    warning = 1;
                    printf("Some words have been skipped because they exceed WORDSIZE (%d)\n"
                           "It isn't an error because they're usually words we don't want\n"
                           "You can still increase WORDSIZE in lectureLigne.c, kCross.c and calculScore.c\n\n", WORDSIZE);
                }

                while (content[i] == ' ' || content[i] == '\t') i++; //si on est dans un espace on avance
            }
        }

    }



    free(mot);
    fclose(file);
    return t;
}

DoubleTableHachage remplirDoubleTableValidation(char *filePath, int caseSensitive, int blockSize, int outBlock)
{
    //Ouverture du fichier
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);
    char content[BUFFERSIZE];
    //taille aléatoire a redefinir plus tard
    DoubleTableHachage t = doubleTableHachage_creer(TABLEHACHAGESIZE);

    char *mot = malloc(WORDSIZE * sizeof(char)); //taille acceptable pour un mot
    int lineCount = 0;

    while (fgets(content, sizeof(content), file) && lineCount<outBlock*blockSize) { // Lecture ligne par ligne dans la première partie du fichier

        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            doubleTableHachage_supprimer(t);
            free(mot);
            errPrint(-1, "Please increase BUFFERSIZE in kCross.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        int i=0; //compteur pour avancer dans la ligne

        if (content[i]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            double value;
            value = (int) content[i] - '0';//on recupere la note de la review sous la forme d'un int
            i += 2; //on passe au prochain mot


            while (content[i] != '\0') {
                int k = 0;

                while ((content[i] != ' ') && (content[i] != '\t') && (content[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne

                    mot[k] = (char)(caseSensitive ? content[i] : tolower(content[i]));

                    i++;
                    k++;

                }
                mot[k] = '\0'; //on a recuperer un mot complet

                ELEMENT motOld = doubleTableHachage_get(mot, t);
                if (motOld == NULL) {
                    ELEMENT motAjout = malloc(sizeof(struct elem));
                    motAjout->clef = mot;
                    mot = malloc(WORDSIZE * sizeof(char)); // On fait un nouveau malloc (ça évite de faire un malloc pour chaque nouveau mot)
                    motAjout->valeur = value;
                    motAjout->occurence = 1;
                    doubleTableHachage_ajout(motAjout, t);
                } else { //Si le mot était déjà présent
                    //On calcule et on remplace son nouveau nombre d'occurence et sa nouvelle valeur par moyenne
                    motOld->occurence++;
                    motOld->valeur = (motOld->valeur * (motOld->occurence - 1) + value) / motOld->occurence;
                }

                while (content[i] == ' ' || content[i] == '\t') i++; //si on est dans un espace on avance
            }
        }

        lineCount++; //on avance d'une ligne
    }

    while(fgets(content, sizeof(content), file) && lineCount<(outBlock+1)*blockSize){lineCount++;} //Saut du bloc outBlock

    while (fgets(content, sizeof(content), file)) { // Lecture ligne par ligne jusqu'à la fin du fichier

        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            doubleTableHachage_supprimer(t);
            free(mot);
            errPrint(-1, "Please increase BUFFERSIZE in kCross.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        int i=0; //compteur pour avancer dans la ligne

        if (content[i]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            double value;
            value = (int) content[i] - '0';//on recupere la note de la review sous la forme d'un int
            i += 2; //on passe au prochain mot


            while (content[i] != '\0') {
                int k = 0;

                while ((content[i] != ' ') && (content[i] != '\t') && (content[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne

                    mot[k] = (char)(caseSensitive ? content[i] : tolower(content[i]));

                    i++;
                    k++;

                }
                mot[k] = '\0'; //on a recuperer un mot complet

                ELEMENT motOld = doubleTableHachage_get(mot, t);
                if (motOld == NULL) {
                    ELEMENT motAjout = malloc(sizeof(struct elem));
                    motAjout->clef = mot;
                    mot = malloc(WORDSIZE * sizeof(char)); // On fait un nouveau malloc (ça évite de faire un malloc pour chaque nouveau mot)
                    motAjout->valeur = value;
                    motAjout->occurence = 1;
                    doubleTableHachage_ajout(motAjout, t);
                } else { //Si le mot était déjà présent
                    //On calcule et on remplace son nouveau nombre d'occurence et sa nouvelle valeur par moyenne
                    motOld->occurence++;
                    motOld->valeur = (motOld->valeur * (motOld->occurence - 1) + value) / motOld->occurence;
                }

                while (content[i] == ' ' || content[i] == '\t') i++; //si on est dans un espace on avance
            }
        }

    }



    free(mot);
    fclose(file);
    return t;
}

double ecartMoyen(char* filePath, TableHachage table, int caseSensitive, int countNeutral, int outBlock, int blockSize){
    //Ouverture du fichier
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);
    char content[BUFFERSIZE];

    int lineCount = 0; int i=0; double mean = 0;

    while (fgets(content, sizeof(content), file) && lineCount<outBlock*blockSize) {lineCount++;} //Saut (outBlock-1) premier blocs

    while(fgets(content, sizeof(content), file) && lineCount<(outBlock+1)*blockSize){ //Traitement du bloc étudié
        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            errPrint(-1, "Please increase BUFFERSIZE in kCross.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        if (content[0]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            double trueValue;
            trueValue = (int) content[0] - '0';//on recupere la note de la review sous la forme d'un double

            char content2[BUFFERSIZE];
            strcpy(content2, &content[1]);
            strcpy(content, &content2[1]); //Récupération du commentaire sans le score

            double calcValue;
            calcValue = calculScore(content, table, caseSensitive, countNeutral); //On calcule la valeur du commentaire selon l'algorithme

            mean = i*mean/(i+1)+(calcValue-trueValue)*(calcValue-trueValue)/(i+1); //on ré-évalue la moyenne des écarts
        }
    }

    fclose(file); //fermeture sans parcours des derniers blocs (si ils existent)
    return(mean);
}

double ecartMoyenDouble(char* filePath, DoubleTableHachage table, int caseSensitive, int countNeutral, int outBlock, int blockSize){
    //Ouverture du fichier
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);
    char content[BUFFERSIZE];

    int lineCount = 0; int i=0; double mean = 0;

    while (fgets(content, sizeof(content), file) && lineCount<outBlock*blockSize) {lineCount++;} //Saut (outBlock-1) premier blocs

    while(fgets(content, sizeof(content), file) && lineCount<(outBlock+1)*blockSize){ //Traitement du bloc étudié
        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            errPrint(-1, "Please increase BUFFERSIZE in kCross.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        if (content[0]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            double trueValue;
            trueValue = (int) content[0] - '0';//on recupere la note de la review sous la forme d'un double

            char content2[BUFFERSIZE];
            strcpy(content2, &content[1]);
            strcpy(content, &content2[1]); //Récupération du commentaire sans le score

            double calcValue;
            calcValue = calculScoreDouble(content, table, caseSensitive, countNeutral); //On calcule la valeur du commentaire selon l'algorithme

            mean = i*mean/(i+1)+(calcValue-trueValue)*(calcValue-trueValue)/(i+1); //on ré-évalue la moyenne des écarts
        }
    }

    fclose(file); //fermeture sans parcours des derniers blocs (si ils existent)
    return(mean);
}