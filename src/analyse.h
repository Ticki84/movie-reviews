#ifndef ANALYSE_ANALYSE_H
#define ANALYSE_ANALYSE_H

#include "lectureArgs.h"

void analyse(ArgsParser args);

#endif //ANALYSE_ANALYSE_H
