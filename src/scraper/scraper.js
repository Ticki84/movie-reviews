const puppeteer = require('puppeteer');
const fs = require('fs');
const minId = 1;
const maxId = 209001;

async function scrapeProduct() {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    const stream = fs.createWriteStream("data.txt");

    // Pour annuler la requête lorsque la page est inexistante
    await page.setRequestInterception(true);
    page.on('request', request => {
        if (request.isNavigationRequest() && request.redirectChain().length)
            request.abort();
        else
            request.continue();
    });

    for (let film = minId; film < maxId; film++) {
        if ((film - minId) % 500 === 0) {
            console.log((film - minId) + ' / ' + (maxId - minId) + ' (' + (100*(film - minId)/(maxId - minId)).toFixed(2) + '%)');
        }
        try {
            await page.goto('http://www.allocine.fr/film/fichefilm-' + film + '/critiques/presse/');

            const scores = await page.$x('/html/body/div/main/section/div/section[2]/div[2]/*/div[1]/div/div'); // <=> //*[@class="item hred"]/div[1]/div/div
            const reviews = await page.$x('/html/body/div/main/section/div/section[2]/div[2]/*/p');

            for (let i = 0; i < scores.length; i++) {
                // La note est affichée sous forme d'étoile, mais le nom de la classe indique le nombre d'étoile (n50 = 5, n40 = 4, ...)
                const rawScore = await scores[i].getProperty('className');
                const classNames = await rawScore.jsonValue();
                const score = await parseInt(classNames[12]) - 1; // On récupère le char en 12ème position puis on lui retire 1
                const rawReview = await reviews[i].getProperty('textContent');
                const review2 = await rawReview.jsonValue();
                const review = await review2.trim().replace(/[\n\r]/g, " "); // trim supprime les espaces en début et fin de ligne, on vérifie qu'il ne reste pas d'autre espace
				let rev = "";
				for (var j=0; j<review.length; j++) { // On remplace les caractères différents des caractères ASCII ou accentués par des espaces
					if (review.charCodeAt(j) <= 127 || review.charCodeAt(j) >= 160 && review.charCodeAt(j) <= 255) {
						rev += review.charAt(j);
					}
					else rev += ' ';
				}

                stream.write(score + " " + rev + "\n");
            }
        } catch (e) {}
    }

    stream.end();
    console.log("done");
    browser.close();
}

scrapeProduct();
