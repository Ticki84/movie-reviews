#ifndef LAB1_CASPAR8U_LISTSIMPLECHAIN_H
#define LAB1_CASPAR8U_LISTSIMPLECHAIN_H


typedef char* Clef;
typedef double Valeur;
typedef int Occurence;
struct elem {
    Clef clef;
    Valeur valeur;
    Occurence occurence;
};
typedef struct elem* ELEMENT;
typedef struct cellule {
    ELEMENT val;
    struct cellule *suiv;
} cellule;
typedef cellule* Liste;

Liste list_creer();
int list_estVide(Liste L);
Liste list_ajoutTete(Liste L, ELEMENT e);
void list_ajoutFin(Liste L, ELEMENT e);
Liste list_ajoutN(Liste L, ELEMENT e, int n);
ELEMENT list_rechN(Liste L, Clef cl, int n);
ELEMENT list_rech(Liste L, Clef cl);
Liste list_remove(Liste L, Clef cl);
Liste list_removeN(Liste L, int n);
void list_affiche(ELEMENT e);
void list_visualiser(Liste L);

#endif //LAB1_CASPAR8U_LISTSIMPLECHAIN_H