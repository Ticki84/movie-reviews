#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "lectureLigne.h"
#include "exceptionHandler.h"

#define BUFFERSIZE 1024
#define WORDSIZE 46
#define TABLEHACHAGESIZE 1000
#define DOUBLETABLEHACHAGESIZE 500

TableHachage remplirTable(char *filePath, int caseSensitive)
{
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);
    char content[BUFFERSIZE];
    //taille aléatoire a redefinir plus tard
    TableHachage t = tableHachage_creer(TABLEHACHAGESIZE);

    char *mot = malloc(WORDSIZE * sizeof(char)); //taille acceptable pour un mot

    int warning = 0;

    while (fgets(content, sizeof(content), file)) { // Lecture ligne par ligne

        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            tableHachage_supprimer(t);
            free(mot);
            errPrint(-1, "Please increase BUFFERSIZE in lectureLigne.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        int i=0; //compteur pour avancer dans la ligne

        if (content[i]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            double value;
            value = (int) content[i] - '0';//on recupere la note de la review sous la forme d'un int
            i += 2; //on passe au prochain mot


            while (content[i] != '\0') {
                int k = 0;

                while (k < WORDSIZE && (content[i] != ' ') && (content[i] != '\t') && (content[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne

                    mot[k] = (char)(caseSensitive ? content[i] : tolower(content[i]));

                    i++;
                    k++;

                }
                if (k < WORDSIZE) {
                    mot[k] = '\0'; //on a recuperer un mot complet

                    ELEMENT motOld = tableHachage_get(mot, t);
                    if (motOld == NULL) {
                        ELEMENT motAjout = malloc(sizeof(struct elem));
                        motAjout->clef = mot;
                        mot = malloc(WORDSIZE * sizeof(char)); // On fait un nouveau malloc (ça évite de faire un malloc pour chaque nouveau mot)
                        motAjout->valeur = value;
                        motAjout->occurence = 1;
                        tableHachage_ajout(motAjout, t);
                    } else { //Si le mot était déjà présent
                        //On calcule et on remplace son nouveau nombre d'occurence et sa nouvelle valeur par moyenne
                        motOld->occurence++;
                        motOld->valeur = (motOld->valeur * (motOld->occurence - 1) + value) / motOld->occurence;
                    }
                }
                else if (!warning) // Si on ne peut pas placer le caractère \0 de fin de chaîne
                {
                    warning = 1;
                    printf("Some words have been skipped because they exceed WORDSIZE (%d)\n"
                           "It isn't an error because they're usually words we don't want\n"
                           "You can still increase WORDSIZE in lectureLigne.c, kCross.c and calculScore.c\n\n", WORDSIZE);
                }

                while (content[i] == ' ' || content[i] == '\t') i++; //si on est dans un espace on avance
            }
        }
    }
    free(mot);
    fclose(file);
    return t;
}

DoubleTableHachage remplirDoubleTable(char *filePath, int caseSensitive)
{
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);
    char content[BUFFERSIZE];
    //taille aléatoire a redefinir plus tard
    DoubleTableHachage dt = doubleTableHachage_creer(DOUBLETABLEHACHAGESIZE);

    char *mot = malloc(WORDSIZE * sizeof(char)); //taille acceptable pour un mot

    int warning = 0;

    while (fgets(content, sizeof(content), file)) { // Lecture ligne par ligne

        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            doubleTableHachage_supprimer(dt);
            free(mot);
            errPrint(-1, "Please increase BUFFERSIZE in lectureLigne.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        int i=0; //compteur pour avancer dans la ligne

        if (content[i]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            double value;
            value = (int) content[i] - '0';//on recupere la note de la review sous la forme d'un int
            i += 2; //on passe au prochain mot


            while (content[i] != '\0') {
                int k = 0;

                while (k < WORDSIZE && (content[i] != ' ') && (content[i] != '\t') && (content[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne

                    mot[k] = (char)(caseSensitive ? content[i] : tolower(content[i]));

                    i++;
                    k++;

                }
                if (k < WORDSIZE) {
                    mot[k] = '\0'; //on a recuperer un mot complet

                    ELEMENT motOld = doubleTableHachage_get(mot, dt);
                    if (motOld == NULL) {
                        ELEMENT motAjout = malloc(sizeof(struct elem));
                        motAjout->clef = mot;
                        mot = malloc(WORDSIZE * sizeof(char)); // On fait un nouveau malloc (ça évite de faire un malloc pour chaque nouveau mot)
                        motAjout->valeur = value;
                        motAjout->occurence = 1;
                        doubleTableHachage_ajout(motAjout, dt);
                    } else { //Si le mot était déjà présent
                        //On calcule et on remplace son nouveau nombre d'occurence et sa nouvelle valeur par moyenne
                        motOld->occurence++;
                        motOld->valeur = (motOld->valeur * (motOld->occurence - 1) + value) / motOld->occurence;
                    }
                }
                else if (!warning) { // Si on ne peut pas placer le caractère \0 de fin de chaîne
                    warning = 1;
                    printf("Some words have been skipped because they exceed WORDSIZE (%d)\n"
                           "It isn't an error because they're usually words we don't want\n"
                           "You can still increase WORDSIZE in lectureLigne.c, kCross.c and calculScore.c\n\n", WORDSIZE);
                }

                while (content[i] == ' ' || content[i] == '\t') i++; //si on est dans un espace on avance
            }
        }
    }
    free(mot);
    fclose(file);
    return dt;
}
