#include <string.h>
#include <stdlib.h>
#include "tableHachage.h"


int hachage(char *s, int mod)
{
    int i = 0, cpt = 0;

    while (s[i] != '\0')
    {
        cpt += (int)s[i];
        i++;
    }
    cpt += strlen(s);
    cpt = cpt % mod;
    return cpt;
}



TableHachage tableHachage_creer(int tailleTableau) {
    TableHachage t = malloc(sizeof(tableHachage));
    t->tableauListe = malloc(sizeof(Liste) * tailleTableau);
    for (int i = 0; i < tailleTableau; i++) {
        *(t->tableauListe + i) = list_creer();
    }
    t->tailleTableau = tailleTableau;
    return t;
}

ELEMENT tableHachage_get(Clef c, TableHachage t) {
    int h = hachage(c, t->tailleTableau);
    if (h < 0) return NULL; // Nécessaire pour la base de données en français
    Liste l = *(t->tableauListe + h);
    return list_rech(l, c);
}


void tableHachage_ajout(ELEMENT e, TableHachage t) {
    int h = hachage(e->clef, t->tailleTableau);
    if (h < 0) return; // Nécessaire pour la base de données en français
    Liste l = *(t->tableauListe + h);
    if (list_rech(l, e->clef) != NULL) return;
    list_ajoutFin(l, e);
}

void tableHachage_supprime(Clef c, TableHachage t) {
    int h = hachage(c, t->tailleTableau);
    if (h < 0) return; // Nécessaire pour la base de données en français
    Liste l = *(t->tableauListe + h);
    *(t->tableauListe + h) = list_remove(l, c);
}

void tableHachage_visualiser(TableHachage t) {
    for (int i = 0; i < t->tailleTableau; i++) {
        list_visualiser(*(t->tableauListe + i));
    }
}

void tableHachage_supprimer(TableHachage t) {
    for (int i = 0; i < t->tailleTableau; i++) {
        while (!list_estVide(*(t->tableauListe + i))) {
            *(t->tableauListe + i) = list_removeN(*(t->tableauListe + i), 0);
        }
        free(*(t->tableauListe + i));
    }
    free(t->tableauListe);
    free(t);
}


