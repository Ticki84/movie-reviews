#include "main.h"
#include "lectureArgs.h"
#include "kCross.h"
#include "words.h"
#include "analyse.h"
#include "tests.h"


int main(int argc, char *argv[]) {
    // Lecture des arguments
    ArgsParser args = getArgs(argc, argv);

    // Programme principal
    if (args->analyse) analyse(args);
    else if (args->words) words(args);
    else if (args->validation) kValidation(args);
    else if (args->tests) tests();
    delete(args);

    return 0;
}