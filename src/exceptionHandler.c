#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "exceptionHandler.h"

void errPrint(int errNo, const char *msg) {
    fprintf(stderr, "Error (code %i):\n%s\n", errNo, msg);
    exit(errNo);
}

void errPrintCat(int errNo, const char *msg, const char *toCat)
{
    char str[80];
    strcpy(str, msg);
    strcat(str, toCat);
    errPrint(errNo, str);
}

void helpPrint(int errNo) {
    fprintf(stdout, "This program can get the global feeling of a movie review.\n"
                    "\n"
                    "  Use:\n"
                    "    ./main [-h | --help]\n"
                    "    ./main (a | analyse) <rev> [(-i <ign> | --ignore <ign>)] [(-c | --case)] [-s | --simple] [-f | --factor] [(-n | --neutral)] [com]\n"
                    "    ./main (w | words) <rev> [(-i <ign> | --ignore <ign>)] [(-c | --case)] [-s | --simple] [-f | --factor]\n"
                    "    ./main (v | validate) <rev> [(-k <k> | --k <k>)] [(-b | --blocs)] [(-i <ign> | --ignore <ign>)] [(-c | --case)] [-s | --simple] [-f | --factor] [(-n | --neutral)]\n"
                    "    ./main (t | tests)\n"
                    "\n"
                    "  Commands:\n"
                    "    -h, --help                            Show help and exit.\n"
                    "    a, analyse                            Analyse a comment.\n"
                    "    w, words                              Ask on loop for words and display their score.\n"
                    "    v, validate                           Do a k-cross validation test.\n"
                    "    t, tests                              Do some unit tests.\n"
                    "\n"
                    "  Options:\n"
                    "    -i <ign>, --ignore <ign>              Ignore words in <ign> file [default: NULL].\n"
                    "    -c, --case                            If set, enable case sensitivity for comment, reviews and ignored words.\n"
                    "    -n, --neutral                         If set, all words not found will be counted as neutral.\n"
                    "    -k <k>, --k <k>                       Set the number of samples for k-cross [default: 2].\n"
                    "    -b, --blocs                           If set, show mean error for each k.\n"
                    "    -s, --simple                          If set, use the naive version of hach table.\n"
                    "    -f, --factor                          If set, show the load factor of hash table.\n"
                    "\n"
                    "  Variables:\n"
                    "    rev                                   Path to reviews file.\n"
                    "    ign                                   Path to file with ignored words.\n"
                    "    com                                   The comment to evaluate.\n"
                    "    k                                     The number of samples (k-cross).\n");
    exit(errNo);
}
