#ifndef COMPRESSION_H
#define COMPRESSION_H


double facteurCompressionSimple(TableHachage t) ;
double facteurCompressionDouble(DoubleTableHachage dt) ;

#endif //COMPRESSION_H