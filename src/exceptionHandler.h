#ifndef ANALYSE_EXCEPTIONHANDLER_H
#define ANALYSE_EXCEPTIONHANDLER_H

void errPrint(int errNo, const char *msg);
void errPrintCat(int errNo, const char *msg, const char *toCat);
void helpPrint(int errNo);

#endif //ANALYSE_EXCEPTIONHANDLER_H
