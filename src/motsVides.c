#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "motsVides.h"
#include "exceptionHandler.h"

#define BUFFERSIZE 46

void supprimerMotsVides(TableHachage t, char *filePath, int caseSensitive)
{
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);
    char content[BUFFERSIZE];

    char *mot = malloc(BUFFERSIZE * sizeof(char)); //taille acceptable pour un mot

    while (fgets(content, sizeof(content), file)) { // Lecture ligne par ligne

        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            tableHachage_supprimer(t);
            free(mot);
            errPrint(-1, "Please increase BUFFERSIZE in motsVides.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        int i=0; //compteur pour avancer dans la ligne
        if (content[i]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            while (content[i] != '\0') {
                int k = 0;

                while ((content[i] != ' ') && (content[i] != '\t') && (content[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne

                    mot[k] = (char)(caseSensitive ? content[i] : tolower(content[i]));

                    i++;
                    k++;

                }
                mot[k] = '\0'; //on a recuperer un mot complet

                tableHachage_supprime(mot, t);

                while (content[i] == ' ' || content[i] == '\t') i++; //si on est dans un espace on avance
            }
        }
    }
    free(mot);
    fclose(file);
}

void supprimerMotsVidesDouble(DoubleTableHachage dt, char *filePath, int caseSensitive)
{
    FILE* file = fopen(filePath, "r");
    if (!file) errPrintCat(-1, "Can't open file ", filePath);
    char content[BUFFERSIZE];

    char *mot = malloc(BUFFERSIZE * sizeof(char)); //taille acceptable pour un mot

    while (fgets(content, sizeof(content), file)) { // Lecture ligne par ligne

        if (*(content + strlen(content) - 1) == '\n') *(content + strlen(content) - 1) = '\0';
        else {
            doubleTableHachage_supprimer(dt);
            free(mot);
            errPrint(-1, "Please increase BUFFERSIZE in motsVides.c"); // Si il n'y a pas de retour à la ligne alors le buffer est trop petit
        }

        int i=0; //compteur pour avancer dans la ligne
        if (content[i]!='\0') { // on vérifie qu'on n'est pas en fin de ligne (ligne vide)
            while (content[i] != '\0') {
                int k = 0;

                while ((content[i] != ' ') && (content[i] != '\t') && (content[i] != '\0')) { //on fait sur espace, tab et \0 pour la fin de ligne

                    mot[k] = (char)(caseSensitive ? content[i] : tolower(content[i]));

                    i++;
                    k++;

                }
                mot[k] = '\0'; //on a recuperer un mot complet

                doubleTableHachage_supprime(mot, dt);

                while (content[i] == ' ' || content[i] == '\t') i++; //si on est dans un espace on avance
            }
        }
    }
    free(mot);
    fclose(file);
}

