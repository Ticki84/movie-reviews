#ifndef LECTURELIGNE_H
#define LECTURELIGNE_H

#include "tableHachage.h"
#include "doubleTableHachage.h"

TableHachage remplirTable(char *filePath, int caseSensitive);

DoubleTableHachage remplirDoubleTable(char *filePath, int caseSensitive);

#endif //LECTURELIGNE_H
